db.fruits.count();


// Counts the number of fruits
db.fruits.aggregate( [
   { $group: { _id: "$fruitytooty", stock: { $sum: 1 } } },
   { $project: { _id: 0 } }
] )

// Counts the fruits with at least 20 stock
db.fruits.aggregate(
  [
    {
      $match: {
        stock: {
          $gt: 20
        }
      }
    },
    {
      $count: "$fruitsInStock"
    }
  ]
)

// Average of fruits onSale per supplier
db.fruits.aggregate(
   [
     {
       $group:
          {
            supplier: "$type",
            price: { $avg: "$price" }
          }
     }
   ]
)

// Max
db.fruits.aggregate(
   [
     {
       $group:
         {
           supplier: "$supplier",
           maxQuantity: { $max: "$quantity" }
         }
     }
   ]
)

// Min
db.fruits.aggregate(
   [
     {
       $group:
         {
           supplier: "$supplier",
           minQuantity: { $min: "$quantity" }
         }
     }
   ]
)